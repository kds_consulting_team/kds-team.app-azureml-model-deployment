# Azure ML Model Deployment Tool

This tool allows for deployment of trained Python models packaged via Pickle or Joblib to Azure Container Webservices via Azure ML platform. It creates an API for these models and outputs the Bearer tokens, the URI, and Swagger for the API.

You need to have an active Azure ML account and an active workspace for using this component.

This whole component mimics this workflow - [How to deploy and where](https://docs.microsoft.com/en-us/azure/machine-learning/service/how-to-deploy-and-where). It deploys the model to Azure Container Instances through an existing Workspace. The WebService then accepts and returns JSON data.

### Adjustments

This component will be further enhanced. In the future, there will be these improvements:  
- The tokens will be sent to email  
- The API will be accepting requests in a format that will allow for integration with PowerBI  
- There will be a new component created that will be able to fetch metadata from the API  
- A functionality for updating a webservice  - the idea is that if you will use a name of an existing webservice AND you'll check that you want to update a service (so the chance of this happening by accident is minimized), the component will only update an existing webservice

## Configuration description

### Parameters

**Subscription ID** - your unique subscription ID (for more info click [here](https://docs.bitnami.com/azure/faq/administration/find-subscription-id/))

**Resource Group** - the resource group your Workspace belongs to (can be found [here](https://portal.azure.com/#home))

**Workspace Name** - the name of the workspace you want to use for the deployment

**Workspace ID** - the id of the workspace you want to use for the deployment

**Model Name** - the name your model will be registered under

**Dependencies in the Inference Config Yaml** - dependencies of your model and your score.py function (for more info click [here](https://docs.microsoft.com/en-us/azure/machine-learning/service/how-to-deploy-and-where#2-define-your-inferenceconfig))

**PIP command for the Inference Config Yaml** - pip commands that will be executed during deployment of your model as a Web Service (for more info click [here](https://docs.microsoft.com/en-us/azure/machine-learning/service/how-to-deploy-and-where#2-define-your-inferenceconfig))

**CPU CORES** - how many CPU cores will your model run on

**MEMORY GB** - how much memory will your model have access to. Please use multiples of 0.1

**Authorization** - BOOLEAN denoting whether the API will require authentication (it is strongly recommended to use Authentication, the component as of now does not allow for not using Authentication - please use only the value `true` for now)

**Name of the WebService** - the name under which your webservice will be deployed

**Email for sending the config** - NOT ACTIVE YET (you will get an email to this address with the URI, API keys and the link to documentation).

**Tenant ID for authentication** - used for the ServicePrincipalAuthentication (read more [here](https://docs.microsoft.com/en-us/python/api/azureml-core/azureml.core.authentication.serviceprincipalauthentication?view=azure-ml-py))

**Service principal ID for authentication** - used for the ServicePrincipalAuthentication (read more [here](https://docs.microsoft.com/en-us/python/api/azureml-core/azureml.core.authentication.serviceprincipalauthentication?view=azure-ml-py))

**Service principal password for authentication** - used for the ServicePrincipalAuthentication (read more [here](https://docs.microsoft.com/en-us/python/api/azureml-core/azureml.core.authentication.serviceprincipalauthentication?view=azure-ml-py))

### Input mapping

The input mapping requires two files:

- **model** : this file needs to be a pickled python model (packaged vbia joblib or pickle) and needs to have `.pkl` suffix
- **score.py**: this file is the entry function in the API. It takes what is sent to the API, runs the model, and returns the outputs back to the API. For more info read about `entry script` [here](https://docs.microsoft.com/en-us/azure/machine-learning/service/how-to-deploy-and-where#prepare-to-deploy)

### Output mapping

There is only one file outputted, it contains info about the deployed Web Service. The name is always compiled as `configuration_` + `[Name of the WebService]` + `.txt` (e.g. is your webservice is called `test`, the output file is called `configuration_test.txt`).
