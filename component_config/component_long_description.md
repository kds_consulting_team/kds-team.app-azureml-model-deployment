# Azure ML Model Deployment Tool

This tool allows for deployment of trained Python models packaged via Pickle or Joblib to Azure Container Webservices via Azure ML platform. It creates an API for these models and outputs the Bearer tokens, the URI, and Swagger for the API.

You need to have an active Azure ML account and an active workspace for using this component.

This whole component mimics this workflow - [How to deploy and where](https://docs.microsoft.com/en-us/azure/machine-learning/service/how-to-deploy-and-where). It deploys the model to Azure Container Instances through an existing Workspace. The WebService then accepts and returns JSON data.

### Adjustments

This component will be further enhanced. In the future, there will be these improvements:  
- The tokens will be sent to email  
- The API will be accepting requests in a format that will allow for integration with PowerBI  
- There will be a new component created that will be able to fetch metadata from the API
- A functionality for updating a webservice - the idea is that if you will use a name of an existing webservice AND you'll check that you want to update a service (so the chance of this happening by accident is minimized), the component will only update an existing webservice
