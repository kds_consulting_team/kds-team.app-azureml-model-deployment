### Parameters

**Tenant ID for authentication** - used for the ServicePrincipalAuthentication (Look for `Service Principal Authentication` [here](https://github.com/Azure/MachineLearningNotebooks/blob/master/how-to-use-azureml/manage-azureml-service/authentication-in-azureml/authentication-in-azureml.ipynb))

**Service principal ID for authentication** - used for the ServicePrincipalAuthentication (Look for `Service Principal Authentication` [here](https://github.com/Azure/MachineLearningNotebooks/blob/master/how-to-use-azureml/manage-azureml-service/authentication-in-azureml/authentication-in-azureml.ipynb))

**Service principal password for authentication** - used for the ServicePrincipalAuthentication (Look for `Service Principal Authentication` [here](https://github.com/Azure/MachineLearningNotebooks/blob/master/how-to-use-azureml/manage-azureml-service/authentication-in-azureml/authentication-in-azureml.ipynb))

**Subscription ID** - your unique subscription ID (for more info click [here](https://docs.bitnami.com/azure/faq/administration/find-subscription-id/))

**Resource Group** - the resource group your Workspace belongs to (can be found [here](https://portal.azure.com/#home))

**Workspace Name** - the name of the workspace you want to use for the deployment

**Model Name** - the name your model will be registered under

**Dependencies in the Inference Config Yaml** - dependencies of your model and your score.py function (for more info click [here](https://docs.microsoft.com/en-us/azure/machine-learning/service/how-to-deploy-and-where#2-define-your-inferenceconfig)). The required format is a list delimited by commas (e.g. `pandas, numpy, scikit-learn=0.19.1`).

**PIP command for the Inference Config Yaml** - pip commands that will be executed during deployment of your model as a Web Service (for more info click [here](https://docs.microsoft.com/en-us/azure/machine-learning/service/how-to-deploy-and-where#2-define-your-inferenceconfig)). The required format is a list delimited by commas (e.g. `azureml-defaults==1.0.79, inference-schema[numpy-support]`).

**Name of the WebService** - the name under which your webservice will be deployed

**Authorization** - BOOLEAN denoting whether the API will require authentication (it is strongly recommended to use Authentication, the component as of now does not allow for not using Authentication - please use only the default value `true` for now)

**CPU CORES** - how many CPU cores will your model run on

**MEMORY GB** - how much memory will your model have access to. Please use multiples of 0.1

### Input mapping

The input mapping requires two files:

- **model** : this file needs to be a pickled python model (packaged vbia joblib or pickle) and needs to have `.pkl` suffix
- **score.py**: this file is the entry function in the API. It takes what is sent to the API, runs the model, and returns the outputs back to the API. For more info read about `entry script` [here](https://docs.microsoft.com/en-us/azure/machine-learning/service/how-to-deploy-and-where#prepare-to-deploy)

### Output

There is only one file outputted, it contains info about the deployed Web Service. The name is always compiled as `configuration_` + `[Name of the WebService]` + `.txt` (e.g. is your webservice is called `test`, the output file is called `configuration_test.txt`).
