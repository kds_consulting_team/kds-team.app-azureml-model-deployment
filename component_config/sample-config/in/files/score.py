
import numpy as np

from sklearn.externals import joblib
import pickle

from inference_schema.schema_decorators import input_schema, output_schema
from inference_schema.parameter_types.numpy_parameter_type import NumpyParameterType

from azureml.core.model import Model
import logging
logging.basicConfig(level=logging.DEBUG)
print(Model.get_model_path(model_name='MODEL_NAME_PLACEHOLDER'))


def init():
    global model
    # AZUREML_MODEL_DIR is an environment variable created during deployment.
    # Join this path with the filename of the model file.
    # It holds the path to the directory that contains the deployed model
    # (./azureml-models/$MODEL_NAME/$VERSION).
    # If there are multiple models, this value is the path to the directory containing all deployed models
    # (./azureml-models).
    model_path = Model.get_model_path('MODEL_NAME_PLACEHOLDER')
    # Deserialize the model file back into a sklearn model
    try:
        model = pickle.load(open(model_path, 'rb'))
    except pickle.UnpicklingError:
        model = joblib.load(model_path)


input_sample = np.array([[10, 9, 8, 7, 6, 5, 4, 3, 2, 1]])
output_sample = np.array([3726.995])


@input_schema('data', NumpyParameterType(input_sample))
@output_schema(NumpyParameterType(output_sample))
def run(data):
    try:
        result = model.predict(data)
        # You can return any data type, as long as it is JSON serializable.
        return result.tolist()
    except Exception as e:
        error = str(e)
        return error
