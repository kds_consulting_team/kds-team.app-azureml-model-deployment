'''
Template Component main class.

'''

import logging
import os
import sys
import json

from kbc.env_handler import KBCEnvHandler

from azureml.core import Workspace
from azureml.core.model import Model
from azureml.core.model import InferenceConfig
from azureml.core.webservice import AciWebservice
from azureml.core.authentication import ServicePrincipalAuthentication
from azureml.exceptions import UserErrorException
from azureml.exceptions import AzureMLException
from azureml.exceptions import WebserviceException
from msrest.exceptions import AuthenticationError

# configuration variables
# workspace creation
SUBSCRIPTION_ID = 'subscription_id'
RESOURCE_GROUP = 'resource_group'
WORKSPACE_NAME = 'workspace_name'

# authentication
TENANT_ID = "tenant_id"
SERVICE_PRINCIPAL_ID = "service_principal_id"
SERVICE_PRINCIPAL_PASSWORD = "#service_principal_password"

# model registering
MODEL_NAME = 'model_name'

# YAML for InferenceConfig
DEPS = 'dependencies'
PY = 'pip_yaml'

# Deployment
CPU_CORES = 'cpu_cores'
MEMORY_GB = 'memory_gb'
AUTH = 'auth'
WEBSERVICE_NAME = 'webservice_name'

# #### Keep for debug
KEY_STDLOG = 'stdlogging'
KEY_DEBUG = 'debug'
MANDATORY_PARS = []
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.2.0'

sys.tracebacklimit = 0


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)

        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True

        log_level = logging.DEBUG if debug else logging.INFO
        # setup GELF if available
        if os.getenv('KBC_LOGGER_ADDR', None):
            self.set_gelf_logger(log_level)
        else:
            self.set_default_logger(log_level)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

        self.SUBSCRIPTION_ID = self.cfg_params[SUBSCRIPTION_ID]
        self.RESOURCE_GROUP = self.cfg_params[RESOURCE_GROUP]
        self.WORKSPACE_NAME = self.cfg_params[WORKSPACE_NAME]

        self.TENANT_ID = self.cfg_params[TENANT_ID]
        self.SERVICE_PRINCIPAL_ID = self.cfg_params[SERVICE_PRINCIPAL_ID]
        self.SERVICE_PRINCIPAL_PASSWORD = self.cfg_params[SERVICE_PRINCIPAL_PASSWORD]

        self.MODEL_PATH = [i for i in os.listdir(
            '/data/in/files/') if '.pkl' in i and 'manifest' not in i][0]
        self.SCORE_PATH = [i for i in os.listdir(
            '/data/in/files/') if 'score.py' in i and 'manifest' not in i][0]

        self.MODEL_NAME = self.cfg_params[MODEL_NAME]

        self.DEPS = self.cfg_params[DEPS]
        self.PY = self.cfg_params[PY]

        # Deployment
        self.CPU_CORES = self.cfg_params[CPU_CORES]
        self.MEMORY_GB = self.cfg_params[MEMORY_GB]
        self.AUTH = self.cfg_params[AUTH]
        self.WEBSERVICE_NAME = self.cfg_params[WEBSERVICE_NAME]

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa

        if not self.AUTH:
            auth_invalid = True
        else:
            auth_invalid = False

        if self.SERVICE_PRINCIPAL_PASSWORD.strip() == '':
            spp_invalid = True
        else:
            spp_invalid = False

        if self.SERVICE_PRINCIPAL_ID.strip() == '':
            spi_invalid = True
        else:
            spi_invalid = False

        if self.TENANT_ID.strip() == '':
            ti_invalid = True
        else:
            ti_invalid = False

        if sum([auth_invalid, spp_invalid, spi_invalid, ti_invalid]) > 0:
            if auth_invalid:
                logging.error(
                    'Please set the Authorization parameter to true.')
            if spp_invalid:
                logging.error('Service Principal Password is invalid.')
            if spi_invalid:
                logging.error('Service Principal ID is invalid.')
            if ti_invalid:
                logging.error('Tenant ID is invalid.')

            sys.exit(1)

        logging.info('Creating workspace...')
        logging.info('SUBSCRIPTION_ID ' + self.SUBSCRIPTION_ID)

        svc_pr = ServicePrincipalAuthentication(
            tenant_id=self.TENANT_ID,
            service_principal_id=self.SERVICE_PRINCIPAL_ID,
            service_principal_password=self.SERVICE_PRINCIPAL_PASSWORD)

        try:
            ws = Workspace(subscription_id=self.SUBSCRIPTION_ID,
                           resource_group=self.RESOURCE_GROUP,
                           workspace_name=self.WORKSPACE_NAME,
                           auth=svc_pr,
                           _location=None,
                           _disable_service_check=False,
                           sku='basic')
            logging.info("Workspace {} loaded.".format(self.WORKSPACE_NAME))

        except UserErrorException as e:
            logging.error('{}'.format(e.message))
            sys.exit(1)

        except AzureMLException as e:
            message_dict = json.loads(e.message)
            logging.error('The status code is {}, {}.'.format(
                message_dict['status_code'], message_dict['error_details']['error']['code']))
            logging.error('{}'.format(
                message_dict['error_details']['error']['message']))
            logging.error('Further info might be found on {}.'.format(
                message_dict['url']))
            sys.exit(1)
        except AuthenticationError as e:
            dict_mes = json.loads(str(e)[str(e).find("{"):str(e).find("}")+1])
            logging.error('{}'.format(dict_mes['error_description']))
            sys.exit(1)

        # supressing the print from azureml.core.Model.register() method
        class HiddenPrints:
            def __enter__(self):
                self._original_stdout = sys.stdout
                sys.stdout = open(os.devnull, 'w')

            def __exit__(self, exc_type, exc_val, exc_tb):
                sys.stdout.close()
                sys.stdout = self._original_stdout

        with HiddenPrints():
            model = Model.register(workspace=ws,
                                   model_path='/data/in/files/{}'.format(
                                       self.MODEL_PATH),
                                   model_name=self.MODEL_NAME,
                                   description="")

        logging.info('Model {} registered.'.format(self.MODEL_NAME))

        DEPENDENCIES = [i.strip() for i in self.DEPS.split(',')]
        PIP_YAML = [i.strip() for i in self.PY.split(',')]

        # if there are no packages to be added to the YAML, the list must be empty
        if PIP_YAML[0] == '':
            PIP_YAML = []

        # if there are no dependencies to be added to the YAML, the list must be empty
        if DEPENDENCIES[0] == '':
            DEPENDENCIES = []

        # create the YAML file for Inference Config
        with open('myenv.yml', mode='wt', encoding='utf-8') as out_file:
            out_file.write('name: project_environment\ndependencies:\n')
            for dep in DEPENDENCIES:
                out_file.write('  - {}\n'.format(dep))
            out_file.write('  - pip:')
            for pckg in PIP_YAML:
                out_file.write('\n    - {}'.format(pckg))

        logging.info('YAML for InferenceConfig parsed.')

        # Read in the score.py file
        with open('/data/in/files/{}'.format(self.SCORE_PATH), 'r') as file:
            filedata = file.read()

        # Replace the target string
        filedata = filedata.replace('MODEL_NAME_PLACEHOLDER', self.MODEL_NAME)

        # Write the file out again
        with open('/data/in/files/score.py', 'w') as file:
            file.write(filedata)

        inference_config = InferenceConfig(runtime="python",
                                           entry_script="/data/in/files/score.py",
                                           conda_file="myenv.yml")

        deployment_config = AciWebservice.deploy_configuration(
            cpu_cores=self.CPU_CORES, memory_gb=self.MEMORY_GB, auth_enabled=self.AUTH)

        # we don't want the azureml.core.webservice to log anything under CRITICAL
        logging.getLogger('azureml.core.webservice').setLevel(50)

        logging.info("Inference and Deployment configs are ready.")

        logging.info('Deploying webservice {}...'.format(self.WEBSERVICE_NAME))
        try:
            service = Model.deploy(ws, self.WEBSERVICE_NAME, [
                model], inference_config, deployment_config)
        except WebserviceException as e:
            if e.message[0:43] == 'Error, there is already a service with name':
                logging.error(
                    'The name {} is already taken. Please choose a name that is not used yet\
                         - a new webservice will be deployed then.'.format(self.WEBSERVICE_NAME))
                sys.exit(1)
            else:
                logging.error('{}'.format(e.message))
                sys.exit(1)

        service.wait_for_deployment(show_output=False)
        service.get_logs()

        # outputting the information about the WebService
        with open('/data/out/files/configuration_{}.txt'.format(self.WEBSERVICE_NAME),
                  mode='wt',
                  encoding='utf-8') as out_file:
            out_file.write('Hello,\n\nthe API keys for your new service on ACI are:\
        \nKey 1: {}\nKey 2: {}\nThe URI is: {}\nThe documentation is here: {}\n\nBest regards,\nKeboola'.format(
                service.get_keys()[0], service.get_keys()[1], service.scoring_uri, service.swagger_uri))

        data = {
            "is_public": False,
            "is_permanent": True,
            "is_encrypted": True,
            "notify": False,
            "tags": [
                "message_azureml_model"
            ]
        }

        with open('/data/out/files/configuration_{}.txt.manifest'.format(self.WEBSERVICE_NAME),
                  mode='wt',
                  encoding='utf-8') as outfile:
            json.dump(data, outfile)

        '''
        # send emails with the API tokens
        with open('message.txt', mode='wt', encoding='utf-8') as out_file:
            out_file.write('Hello,\n\nthe API keys for your new service on ACI are:\
                \nKey 1: {}\nKey 2: {}\nThe URI is: {}\n\n\
                Best regards,\nKeboola'.format(
                service.get_keys()[0], service.get_keys()[1], service.scoring_uri))

        bash_command = 'mail -s "API Keys" {} < message.txt'.format(
            self.NOTIFICATION_EMAIL)
        subprocess.call(bash_command, shell=True)
        '''


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = False
    try:
        logging.info('Initializing component...')
        comp = Component(debug)
        logging.info('Running component...')
        comp.run()
    except Exception as e:
        logging.exception(e)
        exit(1)
